package com.example.service;

import java.util.*;
import org.springframework.stereotype.Service;
import com.example.model.Books;

@Service
public class BooksService
{
    private static final Map<Integer, Books> booksMap = new HashMap<Integer, Books>();
    static {
        initBooks();
    }

    private static void initBooks() {
        Books book1 = new Books(1, "Harry Potter", "J.K. Rowling", 500);
        Books book2 = new Books(2, "Narnia", "C.S. Lewis", 200);
        Books book3 = new Books(3, "Sapiens", "Yuval Noah Harari", 300);

        booksMap.put(book1.getBookid(), book1);
        booksMap.put(book2.getBookid(), book2);
        booksMap.put(book3.getBookid(), book3);
    }
    //getting a specific record
    public Books getBooksById(int id) {
        return booksMap.get(id);
    }

    //saving a specific record
    public void saveOrUpdate(Books books) {
        booksMap.put(books.getBookid(), books);
    }

    //deleting a specific record
    public void delete(int id) {
        booksMap.remove(id);
    }

    //updating a specific record
    public void update(Books books, int bookid) {
        booksMap.put(bookid, books);
    }

    //retrieving all records
    public List<Books> getAllBooks() {
        Collection<Books> c = booksMap.values();
        List<Books> list = new ArrayList<Books>();
        list.addAll(c);
        return list;
    }
}